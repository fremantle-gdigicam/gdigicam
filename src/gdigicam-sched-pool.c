/*
 * This file is part of GDigicam
 *
 * Copyright (C) 2008-2009 Nokia Corporation and/or its
 * subsidiary(-ies).
 *
 * Contact: Alexander Bokovoy <alexander.bokovoy@nokia.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * version 2.1 as published by the Free Software Foundation.
 *
 * This library is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 */


/**
 * SECTION:gdigicam-sched-pool
 * @short_description: This component implements the #GstTaskPool
 * interface, to provide a custom threads pool to manage thread
 * scheduling capabilities.
 *
 * Threads created to perform the camerabin internal tasks are
 * automatically managed by the general GStreamer #GstTaskPool
 * structure, mainly to speed up the threads creation.
 *
 * In order to define custom threads scheduling policies, it's
 * important to keep the scheduled threads out of the general
 * GStreamer pool, avoiding those threads to be used for other
 * purposes. the specific scheduling mechanisms should be defined for
 * specific tasks (ex. video source).
 *
 * To enable the real time capabilities, the specific thread to manage
 * will be named with a specific identifier. After that, some low
 * level subcomponent in charge of the policy rules over all the
 * system components should detect this thread creation and set the
 * appropriated scheduling mechanisms for such thread.
 *
 * #GDigicamSchedPool is the #GstTaskPoll object to any digicam like
 * GStreamer bin.
 **/


#include <string.h>
#include <pthread.h>
#include <sys/prctl.h>

#include "gdigicam-sched-pool.h"
#include "gdigicam-debug.h"


/**
 * G_DIGICAM_THREAD_RT_NAME:
 *
 * Name to identify the current thread to be scheduled with a real
 * time priority mechanisms.
 */
#define G_DIGICAM_THREAD_RT_NAME "camera"

/**
 * G_DIGICAM_THREAD_REG_NAME:
 *
 * Name to identify the current thread to be scheduled with a regular
 * priority mechanisms.
 */
#define G_DIGICAM_THREAD_REG_NAME "camera-ui"


/***************************************/
/* GObject support function prototypes */
/***************************************/


static void _g_digicam_sched_pool_class_init (GDigicamSchedPoolClass * klass);
static void _g_digicam_sched_pool_init (GDigicamSchedPool * pool);
static void _g_digicam_sched_pool_finalize (GObject * object);

typedef struct {
    pthread_t thread;
} GDigicamSchedPoolThreadId;

G_DEFINE_TYPE (GDigicamSchedPool, _g_digicam_sched_pool, GST_TYPE_TASK_POOL);


/***********************************************/
/* Abstract function implementation prototypes */
/***********************************************/


static void _default_prepare (GstTaskPool * pool, GError ** error);
static void _default_cleanup (GstTaskPool * pool);
static gpointer _default_push (GstTaskPool * pool, GstTaskPoolFunction func,
                               gpointer data, GError ** error);
static void _default_join (GstTaskPool * pool, gpointer id);


/****************************/
/* Public functions         */
/****************************/


/**
 * g_digicam_sched_pool_new:
 *
 * Creates a new #GDigicamSchedPool object.
 *
 * Returns: The newly created #GDigicamSchedPool object
 **/
GstTaskPool *
g_digicam_sched_pool_new (void)
{
    GstTaskPool *pool;


    G_DIGICAM_DEBUG ("GDigicamSchedPool::g_digicam_sched_pool_new: "
                     "Creating a new GDigicamSchedPool object ...");

    pool = g_object_new (G_DIGICAM_TYPE_SCHED_POOL, NULL);

    return pool;
}


/**
 * g_digicam_sched_pool_rt_identity:
 *
 * Identifies the current thread to be scheduled with Real Time
 * priority mechanism, changing the name of the thread executing this
 * function to the #G_DIGICAM_THREAD_RT_NAME identifier, so it can
 * trigger the priority scheduler which, is listening to thread
 * creation events.
 **/
void
g_digicam_sched_pool_rt_identity (void)
{
    gchar buf[128];
    char prname[16];


    G_DIGICAM_DEBUG ("GDigicamSchedPool::g_digicam_sched_pool_rt_identity: "
                     "Setting real time identity to the current thread ...");

    memset (prname, 0, 16);
    strncpy (prname, G_DIGICAM_THREAD_RT_NAME, 15);

    /* Set new name. */
    if (prctl (PR_SET_NAME, prname)) {
        strerror_r (errno, buf, 128);
        G_DIGICAM_WARN ("GDigicamSchedPool::g_digicam_sched_pool_rt_identity: "
                        "Setting real time identity FAILED: %s - "
                        "Impossible to assign a new thread name: %s",
                        buf, G_DIGICAM_THREAD_RT_NAME);
    } else {
        G_DIGICAM_DEBUG ("GDigicamSchedPool::g_digicam_sched_pool_rt_identity: "
                         "The real time identity was successfully set.");
    }
}


/**
 * g_digicam_sched_pool_reg_identity:
 *
 * Identifies the current thread to be scheduled with Regular priority
 * mechanism, changing the name of the thread executing this function
 * to the #G_DIGICAM_THREAD_REG_NAME identifier, so it can trigger the
 * priority scheduler which, is listening to thread creation events.
 **/
void
g_digicam_sched_pool_reg_identity (void)
{
    gchar buf[128];
    char prname[16];


    G_DIGICAM_DEBUG ("GDigicamSchedPool::g_digicam_sched_pool_reg_identity: "
                     "Setting regular identity to the current thread ...");

    /* Set new name. */
    memset (prname, 0, 16);
    strncpy (prname, G_DIGICAM_THREAD_REG_NAME, 15);
    if (prctl (PR_SET_NAME, prname)) {
        strerror_r (errno, buf, 128);
        G_DIGICAM_WARN ("GDigicamSchedPool::g_digicam_sched_pool_reg_identity: "
                        "Setting regular identity FAILED: %s - "
                        "Impossible to assign a new thread name: %s",
                        buf, G_DIGICAM_THREAD_REG_NAME);
    } else {
        G_DIGICAM_DEBUG ("GDigicamSchedPool::g_digicam_sched_pool_reg_identity: "
                         "The regular identity was successfully set.");
    }
}


/***************************************/
/* Private functions to manage GObject */
/***************************************/


static void
_g_digicam_sched_pool_class_init (GDigicamSchedPoolClass *klass)
{
    GObjectClass *gobject_class;
    GstTaskPoolClass *gsttaskpool_class;


    gobject_class = (GObjectClass *) klass;
    gsttaskpool_class = (GstTaskPoolClass *) klass;

    gobject_class->finalize =
        GST_DEBUG_FUNCPTR (_g_digicam_sched_pool_finalize);

    gsttaskpool_class->prepare = _default_prepare;
    gsttaskpool_class->cleanup = _default_cleanup;
    gsttaskpool_class->push = _default_push;
    gsttaskpool_class->join = _default_join;
}


static void
_g_digicam_sched_pool_init (GDigicamSchedPool *pool)
{
}


static void
_g_digicam_sched_pool_finalize (GObject *object)
{
    G_OBJECT_CLASS (_g_digicam_sched_pool_parent_class)->finalize (object);
}


/****************************************************************/
/* Private functions implementing the abstract public functions */
/****************************************************************/


static void
_default_prepare (GstTaskPool *pool,
                  GError **error)
{

    g_assert (GST_IS_TASK_POOL (pool));

    /* We don't do anything here. We could construct a pool of threads
     * here that we could reuse later but we don't. */
    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_prepare: "
                     "Preparing schedule pool %p .", pool);
}


static void
_default_cleanup (GstTaskPool * pool)
{

    g_assert (GST_IS_TASK_POOL (pool));

    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_cleanup: "
                     "Cleanup schedule pool %p .", pool);
}


static gpointer
_default_push (GstTaskPool * pool,
               GstTaskPoolFunction func,
               gpointer data,
               GError ** error)
{
    GDigicamSchedPoolThreadId *tid = NULL;
    gint res;
    char prname[16];


    g_assert (GST_IS_TASK_POOL (pool));
    g_assert (NULL != func);

    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_push: "
                     "Pushing schedule pool %p, %p .", pool, func);

    tid = g_slice_new0 (GDigicamSchedPoolThreadId);

    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_push: "
                     "Create thread.");

    /* Get current thread name. */
    memset (prname, 0, 16);
    prctl (PR_GET_NAME, prname);
    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_push: "
                     "Current thread name is %s .", prname);

    /* Change the name of the main process while thread */
    /* is being created.. */
    g_digicam_sched_pool_rt_identity ();

    /* Create a new thread. */
    res = pthread_create (&tid->thread, NULL, (void *(*)(void *)) func, data);
    if (0 != res) {
        g_set_error (error, G_THREAD_ERROR, G_THREAD_ERROR_AGAIN,
                     "Error creating thread: %s .", g_strerror (res));
        g_slice_free (GDigicamSchedPoolThreadId, tid);
        tid = NULL;
        G_DIGICAM_ERR ("GDigicamSchedPool::_default_push: "
                       "Error creating thread: %s .",
                       g_strerror (res));
    }

    return tid;
}


static void
_default_join (GstTaskPool * pool,
               gpointer id)
{
    GDigicamSchedPoolThreadId *tid = (GDigicamSchedPoolThreadId *) id;


    g_assert (GST_IS_TASK_POOL (pool));
    g_assert (NULL != id);

    G_DIGICAM_DEBUG ("GDigicamSchedPool::_default_push: "
                     "Joining schedule pool %p .", pool);

    pthread_join (tid->thread, NULL);

    g_slice_free (GDigicamSchedPoolThreadId, tid);
}
