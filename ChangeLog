2010-07-25  Antía Puentes  <apuentes@igalia.com>

	* src/gdigicam-manager.h:
	(pict_done),
	* src/gdigicam-manager.c:
	(_picture_done),
	* examples/gdigicam_manager_test_app.c:
	(_on_image_capture_done):
	Modified the PICT_DONE_SIGNAL handler signature.
	It uses a const gchar *instead of a GString now.

2010-06-02  Javier Fernández  <jfernandez@igalia.com>

	reviewed by: Antía Puentes <apuentes@igalia.com>

	(_g_digicam_manager_class_init): Added new signals to notify
	"reading/writing" and "no space left" errors that happens in
	the lower layers.
	(_g_digicam_manager_bus_callback): Checks the received errors
	and emits the proper signals.
	(_internal_error_recovering): Tries to stop the bin when an error
	in the lower layers happen.
	(_evaluate_transition): Checks if the state change of the bin
	was a success.
	* src/gdigicam-manager.h (struct _GDigicamManagerClass):
	Added class methods for the new signals.

2010-05-17  Javier Fernández  <jfernandez@igalia.com>

	* examples/Makefile.am:
	(gdigicam_manager_test_app_CFLAGS):
	(gdigicam_manager_test_app_LDADD): Adding the ext subdirectory
	to the linking search path to allow using directly gst-camerabin
	symbols in the sample program.
	* src/gdigicam-manager.c: Conditional compilation
	depending on the platform.
	* ext/gst-camerabin/gdigicam-camerabin.c: Conditional
	compilation depending on the platform.
	(g_digicam_camerabin_descriptor_new): The Maemo
	version of GstCameraBin compnent is slightly different
	than the one released in gstreamer-plugins-bad.
	(_g_digicam_camerabin_set_aspect_ratio_resolution)
	(_g_digicam_camerabin_set_picture_metadata): Avoid
	warnings because wrong string format.
	* examples/gdigicam_manager_test_app.c: Conditional
	compilation depending on the platform, using Hildon
	for Maemo and pure Gtk for Gnome.
	Implementation of a more complete example of how to
	use GDigicam component with the GstCamerabin plugin.
	* debian/rules: New env variable, ENABLE_DEBUG, for
	disabling optimization when debug is enabled.
	* configure.ac: Check for the gstreamer-plugins-bad-dev
	package.
	(GCC_FLAGS): Adding the GDIGICAM_CFLAGS, which avoids warnings
	when compiling with optimization.
	* build.sh: Do not generate the gtk-doc when using
	the build.sh script for configuring.

2010-03-10  Andrés Gómez  <agomez@igalia.com>

	reviewed by: Antía Puentes <apuentes@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c:
	(_g_digicam_camerabin_set_picture_metadata),
	(_g_digicam_camerabin_set_video_metadata): Added support to
	metadata keywords.
	(_foreach_slist_add_keyword_tag): Added. Utility function to merge
	into a tag list the keywords included in a GSList.
	* ext/gst-camerabin/gdigicam-camerabin.h: Added a new member to
	the GDigicamCamerabinMetadata structure in order to store a list
	of keywords.

2010-02-10  Javier Fernández  <jfernandez@igalia.com>

	reviewed by: Andrés Gómez <agomez@igalia.com>

	* doc/reference/gdigicam-docs.sgml: Updated gtk-doc.
	* doc/reference/gdigicam-sections.txt: Updated gtk-doc.
	* ext/gst-camerabin/gdigicam-camerabin.c:
	(_g_digicam_camerabin_handle_bus_message): Check if the received
	message has a valid structure.
	(_g_digicam_camerabin_handle_sync_bus_message): Check if the
	received message has a valid structure. Checking also if the
	message comes from the "videosrc0" component and in that case,
	call to the new defined handler to enable the RT scheduling.
	(_handle_stream_status_message): Added. This handler is used to
	trigger STREAM status messages on the GstBus and change the
	process name when a new thread is created by the 'videosrc'
	component so it would be possible to enable the RT scheduling in
	that specific thread.
	* src/Makefile.am: Added new files to compilation.
	* src/gdigicam-manager-private.h: Added a new private field to
	store the custom GstTaskPool implemented by the GDigicam component
	to store the RT scheduled threads.
	* src/gdigicam-manager.h:
	* src/gdigicam-manager.c:
	(_g_digicam_manager_add_task): Added. New protected API method to
	store threads in the custom GstTaskPool.
	(_g_digicam_manager_rt_identity): Added. New protected API method
	to name the the new created threads in a different way so they
	will be handled by the "Policy" subsytem to enable the real time
	scheduling on them.
	(_g_digicam_manager_reg_identity): Added. New protected API method
	to name the new created threads in the regular way so they will be
	handled by the "Policy" subsystem normally.
	(_g_digicam_manager_init): Initialize the custom GstTaskPool
	private field.
	(_g_digicam_manager_sync_bus_callback): Allow messages with type
	different than GST_MESSAGE_ELEMENT to be handled by the sync_bus
	handler.
	* src/gdigicam-sched-pool.h: Added.
	* src/gdigicam-sched-pool.c: Added.
	* tests/Makefile.am: Added new file to compilation.
	* tests/check-gdigicam-sched-pool.c: Added.
	* tests/check_test.c:
	(configure_tests): Added new tests suite for the sched-pool file.
	* tests/test_suites.h: Added new tests suite for the sched-pool
	file.
	Fixes: NB#140871.

2010-02-02  Andrés Gómez  <agomez@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c: The 16x9 still picture
        resolutions are now 2560x1440.
        Fixes: NB#150488.

2009-11-20  Andrés Gómez  <agomez@igalia.com>

	* doc/reference/gdigicam-sections.txt: Added new gtk-doc.
	* src/gdigicam-manager-private.h: Removed "gst_bus" private
	member.
	* src/gdigicam-manager.h:
	* src/gdigicam-manager.c:
	(g_digicam_manager_set_gstreamer_bin): Removed management of the
	old "gst_bus" private member. Viewfinder sink stored in the proper
	descriptor, now.
	(g_digicam_manager_play_bin): We store the xwindow id just if the
	descriptor has viewfinder capabilities, now.
	(g_digicam_manager_get_xwindow_id): Added. Retrieves the xwindow
	id to be used as output by the gstreamer bin.
	(_g_digicam_manager_init): Removed "gst_bus" private
	member.
	(_g_digicam_manager_cleanup_bin): Removed "gst_bus" private
	member and handler.
	(_g_digicam_manager_cleanup_sink): Resetting "xwindow_id" private
	member.
	(_g_digicam_manager_free_private): Resets the bin related private
	members.
	* tests/check-gdigicam-manager.c:
	(test_start_stop_viewfinder_regular): Removed. Refactored into
	test_start_stop_get_viewfinder_regular.
	(test_start_stop_get_viewfinder_regular): Added. Refactored from
	test_start_stop_viewfinder_regular. It tests the new
	g_digicam_manager_get_xwindow_id API too, now.
	(test_start_stop_viewfinder_invalid): Removed. Refactored into
	test_start_stop_get_viewfinder_invalid.
	(test_start_stop_get_viewfinder_invalid): Added. Refactored from
	test_start_stop_viewfinder_invalid. It tests the new
	g_digicam_manager_get_xwindow_id API too, now.
	(create_g_digicam_manager_suite): test_start_stop_viewfinder
	refactored into test_start_stop_get_viewfinder.

2009-11-03  Andrés Gómez  <agomez@igalia.com>

	* src/gdigicam-manager.c:
	(g_digicam_manager_set_exposure_mode): Fixed wrong assignation on
	exposure related values.

2009-10-27  Andrés Gómez  <agomez@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c
	(g_digicam_camerabin_element_new): Checked for quality property on
	the image encoder element to avoid warnings and added new
	debugging messages.

2009-10-27  Javier Fernández  <jfernandez@igalia.com>

	reviewed by: Andrés Gómez <agomez@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c
	(_g_digicam_camerabin_set_video_metadata): The video geotagging
	metadata should be inserted using an unique tag, called
	GST_TAG_GEO_LOCATION_NAME, instead of using three different tags
	for country, city and suburb.
	Fixes: NB#140871.

2009-10-13  Andrés Gómez  <agomez@igalia.com>

	* ext/gst-camerabin/data/gdigicam-camerabin.conf: Modified default
	JPEG encoder from jpegenc to dspjpegenc.
	Fixes: NB#118043.

	* configure.ac: Removed warnings when not compiling for debugging.

2009-10-09  Andrés Gómez  <agomez@igalia.com>

	* tests/check-gdigicam-manager.c:
	(test_set_get_iso_sensitivity_mode_regular): Replaced the numeric
	value to define.
	(test_set_get_white_balance_mode_limit): Changed the return value
	to check for boolean and error.
	(test_set_get_white_balance_mode_regular): Replaced the numeric
	value to define.
	(test_set_get_focus_mode_invalid): Fixed checking the result
	value.
	(test_set_aspect_ratio_resolution_limit): Fixed the return values
	check.
	(test_set_aspect_ratio_resolution_regular): Corrected the
	expression value for gotten_aspect_ratio.
	Added defines for test values for white balance and ISO
	sensitivity.

	* src/gdigicam-manager.c:
	(g_digicam_manager_descriptor_copy): Fixed wrong copy on supported
	metering modes, overwriting the supported aspect ratios.

2009-09-30  Javier Fernández  <jfernandez@igalia.com>

	reviewed by: Andrés Gómez <agomez@igalia.com>

	* src/gdigicam-manager.c:
	(_picture_done): Fixed memory leak.
	Fixes: NB#137944.

2009-09-30  Andrés Gómez  <agomez@igalia.com>

	* configure.ac: Added "G_DISABLE_CAST_CHECKS" and
	"G_DISABLE_ASSERT" C flags to compilation with optimization.

2009-09-15  Miguel Gómez  <magomez@igalia.com>

	reviewed by: Andrés Gómez <agomez@igalia.com>

	* doc/reference/gdigicam-sections.txt: Added new API for warning
	messages.
	* src/gdigicam-debug.h: Added new macros for warning messages.
	* src/gdigicam-manager.h:
	* src/gdigicam-manager.c:
	(_g_digicam_manager_class_init): Added new "internal-error" signal
	to deal with GStreamer errors.
	(_g_digicam_manager_bus_callback): On GST_MESSAGE_ERROR message in
	the bus callback, set the bin inmediately to NULL state and notify
	any UI.
	Fixes: NB#135225.

2009-09-11  Javier Fernández  <jfernandez@igalia.com>

	reviewed by: Andrés Gómez <agomez@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c:
	(_g_digicam_camerabin_set_picture_metadata),
	(_g_digicam_camerabin_set_video_metadata): Refactored to avoid
	setting uninitialized metadata.
	GST_TAG_DATE_TIME_ORIGINAL and GST_TAG_DATE_TIME_MODIFIED macros
	fixed, as they were swapped.

2009-09-08  Andrés Gómez  <agomez@igalia.com>

	* ext/gst-camerabin/data/gdigicam-camerabin.conf: Modified audio
	bitrate and sample rate.
	* ext/gst-camerabin/gdigicam-camerabin.c
	(g_digicam_camerabin_element_new): Modified audio bitrate and
	sample rate.

2009-09-07  Andrés Gómez  <agomez@igalia.com>

	* ext/gst-camerabin/gdigicam-camerabin.c:
	(_g_digicam_camerabin_handle_bus_message): Process only camerabin
	state changed messages and not the internal element ones. Fixed
	memory leaks.
	(_g_digicam_camerabin_handle_sync_bus_message): Process messages
	depending if they come from camerabin or not.
	Fixes: NB#108662.

2009-09-04  Andrés Gómez  <agomez@igalia.com>

	* doc/reference/gdigicam-docs.sgml:
	* doc/reference/gst-camerabin/gdigicam-gst-camerabin-docs.sgml:
	* ext/gst-camerabin/gdigicam-camerabin.c:
	* ext/gst-camerabin/gdigicam-camerabin.h:
	* src/gdigicam-error.c:
	GTK-Doc completed.
